import React from 'react';
import { Form, FormInput, FormGroup, Button } from "shards-react";

class SearchBar extends React.Component {
    state = {
        searchTerm: ''
    }

    onFormSubmit = (event) => {
        event.preventDefault();
        this.props.onFormSubmit(this.state.searchTerm);
    }

    handleInput = (event) => {
        this.setState({
            searchTerm: event.target.value
        })
    }
    render() {
        return(
        <div>
            <Form onSubmit={this.onFormSubmit}>
                <FormGroup>
                    <label>Video Search</label>
                    <div className='searchField'>
                        <FormInput placeholder="Search" value={this.state.searchTerm} onChange={this.handleInput} />
                        <Button theme="light">Search</Button>
                    </div>
                </FormGroup>
            </Form>
        </div>
        )
    }
}

export default SearchBar;