import React from 'react';
import SearchBar from './SearchBar';
import Youtube from './apis/Youtube'
import VideoList from './VideoList'
import NumberOfVideos from './NumberOfVideos'
import { Container, Row, Col } from "shards-react";
import VideoDetail from './VideoDetail';

class App extends React.Component {
    state = {
        videos: [],
        selectedVideo: null
    }

    onFormSubmit = async term => {
      const response = await Youtube.get('/search', {
            params: {
                part: 'snippet',
                maxResults: 25,
                key: 'AIzaSyD2uzykEhyQMXk9LdPBZqmBvDHtt1qmVJo',
                q: term
            }
        })
        this.setState({
            videos: response.data.items
        })
    }

    onVideoSelect = (video) => {
        this.setState({
            selectedVideo: video
        })
    }

    render() {
        return (
            <Container className="dr-example-container">
                <Row>
                    <Col>
                        <SearchBar onFormSubmit={this.onFormSubmit}/>
                    </Col>
                </Row>
                {this.state.videos.length > 0 &&
                    <Row>
                        <Col>
                            <NumberOfVideos video_results= {this.state.videos.length} />
                        </Col>
                    </Row>
                }
                <Row>
                    {this.state.selectedVideo &&
                        <Col sm="12" lg="8">
                            <VideoDetail video={this.state.selectedVideo} />
                        </Col>
                    }
                    <Col>
                        <VideoList videos={this.state.videos} onVideoSelect={this.onVideoSelect} />
                    </Col>
                </Row>
                
            </Container>
        )
    }
}

export default App;