import React from 'react';
import { Card, CardBody, CardTitle, CardSubtitle } from "shards-react";

const VideoDetail = ({video}) => {
    console.log(video);
    if(!video)
        return <div></div>

    const src = `https://www.youtube.com/embed/${video.id.videoId}`;
    return (
        <div className="video-detail">
            <iframe src={src} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            <Card>
                <CardBody>
                    <CardTitle>{video.snippet.title}</CardTitle>
                    <CardSubtitle>{video.snippet.channelTitle}</CardSubtitle>
                    {video.snippet.description}
                </CardBody>
            </Card>
        </div>
    )
}

export default VideoDetail;