import React from 'react';
import VideoItem from './VideoItem'
import { ListGroup, ListGroupItem } from "shards-react"

const VideoList = ({videos, onVideoSelect}) => {
    const renderedList = videos.map((video, i) => {
        return (
            <ListGroupItem key={i}>
                <VideoItem video={video} onVideoSelect={onVideoSelect} />
            </ListGroupItem>
        )
    });

    return (
            <ListGroup>
                {renderedList}
            </ListGroup>
    )
}

export default VideoList;