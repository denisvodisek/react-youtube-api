import React from 'react';
import { Badge } from "shards-react";

const NumberOfVideos = ({video_results}) => {
    return (
        <div className="no-of-results">
            No. of videos found: <Badge theme="success">{video_results}</Badge>
        </div>
    )
}

export default NumberOfVideos