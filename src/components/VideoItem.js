import React from 'react';
import { Card, CardBody, CardImg, CardTitle, CardSubtitle, Badge } from "shards-react";

const VideoItem = ({video, onVideoSelect}) => {

return (
    <Card onClick={() => onVideoSelect(video)} className="video-card">
        <CardImg top src={video.snippet.thumbnails.medium.url} />
        <CardBody>
            <CardTitle>{video.snippet.title}</CardTitle>
            <CardSubtitle>{video.snippet.channelTitle}</CardSubtitle>
        </CardBody>
    </Card>
)
}

export default VideoItem;