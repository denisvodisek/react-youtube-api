import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import "bootstrap/dist/css/bootstrap.min.css";
import "shards-ui/dist/css/shards.min.css"
import "./assets/style.css"
ReactDOM.render(
    <App />,
    document.querySelector('#root')
);